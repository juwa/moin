[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1436302.svg)](https://doi.org/10.5281/zenodo.1436302) 
[![Travis-CI Build Status](https://travis-ci.org/CRC1266-A2/moin.svg?branch=master)](https://travis-ci.org/CRC1266-A2/moin)

# moin -- MOdelling INteraction.

Using the moin package you can calculate simple location and interaction models. In terms of theoretical models, the package offers gravity and entropy maximization approaches within an algebraic as well as Hamiltonian framework. Empirical models of interaction can be calculated using cultural distance approaches.

This package is the result of the joint work during a [Summer School](https://github.com/ISAAKiel/SummerSchool_moin/) organized by [ISAAKiel](https://isaakiel.github.io/) and [CRC1266](http://www.sfb1266.uni-kiel.de/en/projects/cluster-a-theory-and-modelling/a2-intergrative-modelling?set_language=en). See the DESCRIPTION file for a complete list of authors.

## ToDo

- create release
- update Zenodo release and get new DOI badge
- create vignettes showing how to use the different methods of interaction analyses

## Citation

Waiting for new Zenodo release. 

## Installation

You can install moin from github with:

``` r
if(!require('devtools')) install.packages('devtools')
devtools::install_git("https://gitlab.com/CRC1266-A2/moin")
```

## Licenses

- Text and figures:
  [CC-BY-4.0](http://creativecommons.org/licenses/by/4.0/)
  If figures are created by other people these figures are linked to their source location.

- Code:
  See the [DESCRIPTION](DESCRIPTION) file

- Data:
  [CC-0](http://creativecommons.org/publicdomain/zero/1.0/) attribution requested in reuse
